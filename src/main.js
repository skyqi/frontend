import Vue from 'vue'
import App from './App'
 

Vue.config.productionTip = false

import myUtils from "../util/utils.js";

Vue.prototype.$myUtils = myUtils


App.mpType = 'app'

const app = new Vue({
  ...App,
  beforeCreate() {
     console.log("beforeCreate") ;
  },
    
})
app.$mount()

